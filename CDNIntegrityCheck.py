import hashlib
import sys
import urllib2
import urllib
import requests


print "Copyright (c) 2017 Rohit Samineni"

def GetAkamaiDomain():
	return "office-prod.edgekey.net"
    
def GetAzureDomain():
	return "c2r.a-0020.a-msedge.net"
    
def GetChinaDomain():
	return "officecdn.microsoft.chinacache.net"

def BuildPath(taxRegion, uuid, version, cab):
    retPath = "/"+ taxRegion + "/" + uuid +"/office/data/"
    
    if version == None:
        retPath = retPath + cab
    else:
        retPath = retPath + version + "/" + cab 
    
    return retPath
 
def GetFileContent(domainName, resPath):
    uri = "http://" + domainName + resPath
    heads = {'Host' : 'officecdn.microsoft.com'}
    
    res = None
    
    try:
        res = requests.get(uri, headers = heads)
    except requests.exceptions.TooManyRedirects:
        if domainName == GetAkamaiDomain():
            heads = {'Host' : 'officecdn.microsoft.com.edgesuite.net'}
            res = requests.get(uri, headers = heads)
        else:
            raise
    
    return res.content
    

if len(sys.argv) < 4:
    print "Usage: 'c:\\python27\\python.exe cdnintegritycheck.py [taxRegion] [channeluuid] [cabName] [version]'";
else:
    taxRegion = sys.argv[1]
    channeluuid = sys.argv[2]
    cabname = sys.argv[3]
    version = None
    
    if len(sys.argv) == 5:
        version = sys.argv[4]
    
    path = BuildPath(taxRegion, channeluuid, version, cabname)
    
    akamai = GetFileContent(GetAkamaiDomain(), path)
    print "Akamai SHA256: " + hashlib.sha256(akamai).hexdigest()
    
    azure = GetFileContent(GetAzureDomain(), path)
    print "Azure  SHA256: " + hashlib.sha256(azure).hexdigest()
    
    china = GetFileContent(GetChinaDomain(), path)
    print "China  SHA256: " + hashlib.sha256(china).hexdigest()
    
    if akamai == azure and azure == china:
        print "Integrity verified!"
    else:
        print "Integrity failed!"